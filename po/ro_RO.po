# Romanian translations for test_gettext package
# Traducerea �n limba rom�n� pentru pachetul test_gettext.
# Copyright (C) 2012 Collabora Ltd.
# This file is distributed under the same license as the test_gettext package.
# Automatically generated, 2012.
#
msgid ""
msgstr "ro_RO"
"Project-Id-Version: test_gettext 0.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-06-21 15:20+0200\n"
"PO-Revision-Date: 2012-06-21 15:20+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"

#: test-gettext.c:14
#, c-format
msgid "C"
msgstr "ro_RO"
